<?php
// $Id$

/**
 * Determines role of the owner of a user page
 */
class context_condition_user_pages_role extends context_condition {
  function condition_values() {
    $values = array();
    foreach (user_roles(TRUE) as $rid => $role_name) {
      if ($rid == DRUPAL_AUTHENTICATED_RID) {
        $values['authenticated user'] = check_plain($role_name);
      }
      else {
        $values[$role_name] = check_plain($role_name);
      }
    }
    return $values;
  }
  
  function options_form($context) {
    $defaults = $this->fetch_from_context($context, 'options');
    return array(
      'skip_owner' => array(
        '#title' => t('Skip role evaluation if global user ($user) is viewing their own user page(s)?'),
        '#type' => 'checkbox',
        '#description' => t('Skip role evaluation if the logged in user ($user) is the owner of the user page being viewed. ie. Viewing their own user page(s).'),
        '#default_value' => isset($defaults['skip_owner']) ? $defaults['skip_owner'] : FALSE,
      ),
    );
  }

  function execute() {
    global $user;
    
    //ensure we're on a user page
    if (arg(0) == 'user' && is_numeric(arg(1))) {
      $account = context_user_pages_role_get_account(arg(1));
      if ($roles = $account->roles) {
				foreach ($roles as $rid => $role) {
					foreach ($this->get_contexts($role) as $context) {
						$options = $this->fetch_from_context($context, 'options');
						if ($options['skip_owner'] && $account->uid == $user->uid) {
							return FALSE;
						}
						$this->condition_met($context, $role);
					}
				} 
			}
    }
  }
}