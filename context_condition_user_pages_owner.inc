<?php
// $Id$
define('IS_OWNER', 1);
define('IS_NOT_OWNER', 2);

/**
 * Determines if global user is or is not page owner
 */
class context_condition_user_pages_owner extends context_condition {
  function condition_values() {
    $values = array(
      IS_OWNER => t('Global user is page owner'),
      IS_NOT_OWNER => t('Global user is not page owner')
    );
    return $values;
  }
  
  function condition_form($context) {
    $form = parent::condition_form($context);
    $form['#type'] = 'select';
    $form['#multiple'] = FALSE;
    return $form;
  }

  function condition_form_submit($values) {
    return array($values);
  }

  function execute() {
    global $user;
    if ($this->condition_used()) {
      //ensure we're on a user page
      if (arg(0) == 'user' && is_numeric(arg(1))) {
        $account = context_user_pages_role_get_account(arg(1));
        foreach ($this->get_contexts() as $context) {
          $values = $this->fetch_from_context($context, 'values');
          foreach ($values as $value) {
            //is owner
            if ($account->uid == $user->uid && $value == IS_OWNER) {
              $this->condition_met($context);
            }
            
            //not owner
            if ($account->uid != $user->uid && $value == IS_NOT_OWNER) {
              $this->condition_met($context);
            }
          }
        }
      }
    }
  }
}